package mark.DiaryServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		int[] a = { 1, 2, 3, 4, 5 };
		Stack stack = new Stack(a);

		Stack newStack = stack.reverse(4, new Stack());
		System.out.println(newStack);
	}
}

class Stack {
	private List<Integer> list = new ArrayList<Integer>();

	private int top = 0;

	public int pop() {
		if (top < 0)
			return -1;
		int res = list.get(top);
		list.remove(top--);
		return res;
	}

	public void push(int num) {
		list.add(num);
		top++;
	}

	public Stack() {

	}

	public Stack(int[] nums) {
		for (int i : nums) {
			list.add(i);
		}
		top = list.size() - 1;
	}

	public Stack reverse(int index, Stack stack) {
		if (stack == null) {
			stack = new Stack(null);
		}
		if (index != -1) {

			int res = this.pop();
			stack.push(res);
			stack = reverse(index - 1, stack);
		}
		return stack;

	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (Integer i : this.list) {
			stringBuilder.append(i + " ");
		}
		return stringBuilder.append("\n").toString();
	}

}
package mark.DiaryServer.Config;

public class CURDConfig {
	public static String[] baseDomainInitVarForUpdate = { "domainUuid", "domainRemark" };
	public static String[] textDomainInitVarForUpdate = { "textContent", "anonymousStatus" };
	public static String[] diaryInitVarForUpdate = { "pictureDiary", "diaryWeather", "diaryWood", "diaryTitle",
			"textDomain" };
	public static String[] diaryCommentInitForUpdate = { "textDomain" };

}

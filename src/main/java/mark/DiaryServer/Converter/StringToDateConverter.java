package mark.DiaryServer.Converter;

import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/*
 * 将String转换为Date
 */
public class StringToDateConverter implements Converter<String, Date> {

	@Override
	public Date convert(String source) {
		Long longTime = Long.parseLong(source);
		Date time = new Date(longTime);
		return time;
	}

}

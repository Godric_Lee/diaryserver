package mark.DiaryServer.Dao;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import mark.DiaryServer.Domain.Diary;
import mark.DiaryServer.Domain.DiaryComment;
import mark.DiaryServer.Domain.DiaryDigest;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserRelation;
import mark.DiaryServer.Util.ClassUtil;

public class BaseDao<T> {

	protected LocalSessionFactoryBean localSessionFactoryBean;

	protected Class domainClass;

	protected String tableName;

	protected ClassMetadata classMetaData;

	protected PersistentClass persistentClass;

	@Autowired
	protected HibernateTemplate hibernateTemplate;

	public BaseDao() {
		Type type = this.getClass().getGenericSuperclass();
		ParameterizedType params = (ParameterizedType) type;
		domainClass = (Class) (params.getActualTypeArguments()[0]);
	}

	@Autowired
	public void setLocalSessionFactoryBean(LocalSessionFactoryBean localSessionFactoryBean) {
		this.localSessionFactoryBean = localSessionFactoryBean;
		Configuration configuration = this.localSessionFactoryBean.getConfiguration();
		this.persistentClass = this.getPersistentClass(configuration, domainClass);

		this.tableName = getPersistentClass(configuration, domainClass).getTable().getName();
		System.out.println(domainClass.toString() + "Dao 加载完成，表名:" + this.tableName);
	}

	protected PersistentClass getPersistentClass(Configuration configuration, Class clazz) {
		persistentClass = null;
		if ((persistentClass = configuration.getClassMapping(clazz.getName())) == null) {
			synchronized (configuration) {
				if ((persistentClass = configuration.getClassMapping(clazz.getName())) == null) {
					configuration = configuration.addClass(clazz);
					configuration.buildSessionFactory();
					persistentClass = configuration.getClassMapping(clazz.getName());
				}
			}
		}
		return persistentClass;
	}

	/**
	 * 保存实例
	 * 
	 * @param entity
	 * @return
	 */
	public String save(T entity) {
		return (String) this.hibernateTemplate.save(entity);
	}

	/**
	 * 删除实例
	 * 
	 * @param entity
	 */
	public void delete(T entity) {
		this.hibernateTemplate.delete(entity);
	}

	/**
	 * 更新实例
	 * 
	 * @param entity
	 * @throws Exception
	 */
	public void update(T entity) {
		this.hibernateTemplate.update(entity);
	}

	/**
	 * 查找实例
	 * 
	 * @param id
	 * @return
	 */
	public T get(String id) {
		return (T) this.hibernateTemplate.get(domainClass, id);
	}

	/**
	 * 加载实例
	 * 
	 * @param id
	 * @return
	 */
	public T load(String id) throws ObjectNotFoundException {
		return (T) this.hibernateTemplate.load(domainClass, id);
	}

	/**
	 * 获取Criteria
	 * 
	 * @return
	 */
	public Criteria getCriteria() {
		return this.getSession().createCriteria(this.domainClass);
	}

	/**
	 * 根据示例查询，根据指定顺序排序后分页查询
	 * 
	 * @param entity
	 * @param desc
	 * @param orderKey
	 * @param pageNo
	 * @param pageSize
	 *            从1开始
	 * @return
	 */
	public List<T> queryByExample(T example, boolean desc, List<String> orderKey, int pageNo, int pageSize) {
		Criteria criteria = this.getCriteria();
		criteria.setFirstResult((pageNo - 1) * pageSize);
		criteria.setMaxResults(pageSize);

		HashMap<String, Object> exampleMap = ClassUtil.getExampleMap(null, example, criteria);
		for (Map.Entry<String, Object> entry : exampleMap.entrySet()) {
			criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
		}

		if (orderKey != null) {
			if ((desc && pageSize > 0) || ((!desc) && pageSize > 0)) { // 总体降续 向后翻页||总体升续 向前翻页
				// SELECT * FROM db_books.tb_books order by id desc limit 2;
				for (String key : orderKey) {
					criteria.addOrder(Order.desc(key));
				}

			} else if ((desc && pageSize < 0) || ((!desc) && pageSize < 0)) { // 总体降续 向前翻页||总体升续 向后翻页
				// SELECT * FROM db_books.tb_books where id>=10 order by id asc limit 2;
				for (String key : orderKey) {
					criteria.addOrder(Order.asc(key));
				}
			}
		}

		return criteria.list();
	}

	/**
	 * 根据示例和lastKey查询
	 * 
	 * @param entity
	 * @param desc
	 * @param lastKeyName
	 * @param lastValue
	 * @param pageSize
	 * @return
	 */
	public List<T> queryByExample(T example, boolean desc, HashMap<String, Object> lastKeyValue, int pageSize) {
		Criteria criteria = this.getCriteria();
		criteria.setFirstResult(0);
		if (pageSize != 0)
			criteria.setMaxResults(pageSize);

		HashMap<String, Object> exampleMap = ClassUtil.getExampleMap(null, example, criteria);
		for (Map.Entry<String, Object> entry : exampleMap.entrySet()) {
			criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
		}

		if (lastKeyValue != null) {
			if ((desc && pageSize > 0) || ((!desc) && pageSize > 0)) { // 总体降续 向后翻页||总体升续 向前翻页
				// SELECT * FROM db_books.tb_books where id<=10 order by id desc limit 2;
				for (Map.Entry<String, Object> entry : lastKeyValue.entrySet()) {
					criteria.addOrder(Order.desc(entry.getKey()));
					if (entry.getValue() != null) {
						System.out.println(entry.getKey() + " " + entry.getValue());
						criteria.add(Restrictions.le(entry.getKey(), entry.getValue()));
					}

				}

			} else if ((desc && pageSize < 0) || ((!desc) && pageSize < 0)) { // 总体降续 向前翻页||总体升续 向后翻页
				// SELECT * FROM db_books.tb_books where id>=10 order by id asc limit 2;
				for (Map.Entry<String, Object> entry : lastKeyValue.entrySet()) {
					criteria.addOrder(Order.asc(entry.getKey()));
					if (entry.getValue() != null) {
						criteria.add(Restrictions.ge(entry.getKey(), entry.getValue()));
					}

				}

			}
		}

		return criteria.list();

	}

	/**
	 * 指定实例中的FromUser来查询
	 * 
	 * @param user
	 * @param desc
	 * @param lastDate
	 * @param pageSize
	 * @param pictureDiary
	 * @return
	 */
	public List<T> getByFromUserOrderByTime(User user, boolean desc, Date lastDate, int pageSize,
			Boolean pictureDiary) {
		T example = null;
		try {
			example = (T) Class.forName(this.domainClass.getName()).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		HashMap<String, Object> lastKeyValue = new HashMap<>();

		if (example instanceof Diary) {
			((Diary) example).getTextDomain().setFromUser(user);
			if (pictureDiary != null)
				((Diary) example).setPictureDiary(pictureDiary);
			lastKeyValue.put("textDomain.createTime", lastDate);
		} else if (example instanceof DiaryComment) {
			((DiaryComment) example).getTextDomain().setFromUser(user);
			if (lastDate != null)
				lastKeyValue.put("textDomain.createTime", lastDate);
		} else if (example instanceof DiaryDigest) {
			((DiaryDigest) example).setFromUser(user);
			if (lastDate != null)
				lastKeyValue.put("createTime", lastDate);
		} else if (example instanceof UserRelation) {
			((UserRelation) example).setFromUser(user);
			if (lastDate != null)
				lastKeyValue.put("createTime", lastDate);
		} else {
			return null;
		}
		return this.queryByExample(example, desc, lastKeyValue, pageSize);
	}

	protected List<T> queryByMap(HashMap<Field, String> args, Object object, Object object2) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 根据指定的Map分页查询
	 * 
	 * @param fieldsMap
	 * @param orderKey
	 * @param order
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<T> pageQueryByMap(HashMap<Field, String> fieldsMap, Field orderKey, String order, int pageNo,
			int pageSize) {
		StringBuilder sqlBuilder = new StringBuilder("select * from " + this.tableName);
		sqlBuilder.append(this.addWhereToSQL(fieldsMap));
		sqlBuilder.append(" and ").append(this.getColumnName(orderKey)).append((order.equals("asc") ? ">=" : "<="));
		sqlBuilder.append("(select ").append(this.getColumnName(orderKey)).append(" from ").append(this.tableName);
		sqlBuilder.append(this.addWhereToSQL(fieldsMap));
		sqlBuilder.append(this.addOrderToSQL(orderKey, order));

		int lastIndex = (pageNo - 1) * pageSize;
		sqlBuilder.append(" limit ").append(lastIndex).append(",1)");
		sqlBuilder.append(this.addOrderToSQL(orderKey, order));
		sqlBuilder.append(" limit 0,").append(pageSize);

		HashMap<String, String> args = this.getColumnName(fieldsMap);

		args.putAll(args);
		final Query query = this.createSQLQuery(sqlBuilder.toString(), args);
		return query.list();

	}

	/**
	 * 添加where部分
	 * 
	 * @param fieldsMap
	 * @return
	 */
	public String addWhereToSQL(HashMap<Field, String> fieldsMap) {
		HashMap<String, String> argsMap = this.getColumnName(fieldsMap);

		StringBuilder stringBuilder = new StringBuilder();
		if (argsMap.size() > 0) {
			stringBuilder.append(" where");
			Iterator<Entry<String, String>> iterator = argsMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, String> entry = iterator.next();
				stringBuilder.append(" " + entry.getKey() + " = ? ");
				stringBuilder.append(iterator.hasNext() ? "and" : "");
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * 添加order部分
	 * 
	 * @param orderKey
	 * @param order
	 * @return
	 */
	public String addOrderToSQL(Field orderKey, String order) {
		StringBuilder stringBuilder = new StringBuilder();
		if (orderKey != null) {
			stringBuilder.append(" order by ").append(this.getColumnName(orderKey));
			if (order != null) {
				stringBuilder.append(" " + order);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * 创建Query对象
	 * 
	 * @param sql
	 * @param args
	 * @return
	 */
	public Query createSQLQuery(String sql, HashMap<String, String> args) {
		return this.createSQLQuery(sql, args, domainClass);
	}

	/**
	 * 创建Query对象
	 * 
	 * @param sql
	 * @param args
	 * @param entityClass
	 * @return
	 */
	public Query createSQLQuery(String sql, HashMap<String, String> args, Class entityClass) {
		Query sqlQuery = this.getSession().createSQLQuery(sql).addEntity(entityClass).setCacheable(true);
		int i = 0;
		for (Map.Entry<String, String> entry : args.entrySet()) {
			sqlQuery.setParameter(i++, entry.getValue());
		}
		return sqlQuery;
	}

	public List<T> orderByTime() {
		return null;
	}

	/**
	 * 获取Session
	 * 
	 * @return
	 */
	public Session getSession() {
		return this.hibernateTemplate.getSessionFactory().getCurrentSession();
	}

	/**
	 * 获取Field对应的字段名
	 * 
	 * @param field
	 * @return
	 */
	public String getColumnName(Field field) {
		System.out.println("getColumnName for field:" + field.toString());
		Property property = this.persistentClass.getProperty(field.getName());
		Iterator iterator = property.getColumnIterator();
		return iterator.hasNext() ? (this.tableName + "." + ((Column) iterator.next()).getName()) : null;
	}

	/**
	 * 获取Fields对应的字段名 如果输入为null，返回一个空的HashMap
	 * 
	 * @param fieldsMap
	 * @return
	 */
	public HashMap<String, String> getColumnName(HashMap<Field, String> fieldsMap) {
		HashMap<String, String> argsMap = new HashMap<>();
		if (fieldsMap != null)
			for (Map.Entry<Field, String> entry : fieldsMap.entrySet()) {
				System.out.println(entry.getKey().toString() + " " + entry.getValue().toString());
				argsMap.put(this.getColumnName(entry.getKey()), entry.getValue());
			}
		return argsMap;
	}

}

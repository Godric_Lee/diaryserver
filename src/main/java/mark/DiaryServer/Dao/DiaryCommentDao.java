package mark.DiaryServer.Dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.DiaryComment;
import mark.DiaryServer.Domain.TextDomain;

@Repository
public class DiaryCommentDao extends BaseDao<DiaryComment> {

	@Override
	public String save(DiaryComment diaryComment) {
		TextDomain textDomain = diaryComment.getTextDomain();
		textDomain.setDigestNum(0);
		textDomain.setCommentNum(0);
		textDomain.setCreateTime(new Date());
		this.load(diaryComment.getTextDomain().getFromUser().getDomainUuid());
		System.out.println("----" + diaryComment.getTextDomain().getFromUser().getUserPhone());
		String id = super.save(diaryComment);
		System.out.println("-----------------------");
		return id;
	}

	@Override
	public void update(DiaryComment diaryComment) {
		DiaryComment originalDiaryComment = this.load(diaryComment.getId());
		DiaryComment newDiaryComment = diaryComment;

		newDiaryComment.getTextDomain().setDigestNum(originalDiaryComment.getTextDomain().getDigestNum());
		newDiaryComment.getTextDomain().setCommentNum(originalDiaryComment.getTextDomain().getCommentNum());
		newDiaryComment.getTextDomain().setFromUser(originalDiaryComment.getTextDomain().getFromUser());

		this.update(newDiaryComment);
	}

	/**
	 * 获取TextDomain下的评论，按时间排序
	 * 
	 * @param lastId
	 * @param desc
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<DiaryComment> orderByCreateTime(String lastId, boolean desc, Date lastDate, int pageSize) {

		TextDomain textDomain = new TextDomain();
		textDomain.setDomainUuid(lastId);

		DiaryComment example = new DiaryComment();
		example.setLastComment(textDomain);

		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("textDomain.createTime", lastDate);
		return this.queryByExample(example, desc, lastKeyValue, pageSize);

	}

	/**
	 * 获取TextDomain下的所有评论，按照点赞、评论数排序
	 * 
	 * @param lastId
	 * @param desc
	 * @param lastDegistNum
	 * @param lastCommentNum
	 * @param pageSize
	 * @return
	 */
	public List<DiaryComment> orderByHotNum(String lastId, boolean desc, Integer lastDegistNum, Integer lastCommentNum,
			int pageSize) {
		TextDomain textDomain = new TextDomain();
		textDomain.setDomainUuid(lastId);

		DiaryComment example = new DiaryComment();
		example.setLastComment(textDomain);

		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("textDomain.digestNum", lastDegistNum);
		lastKeyValue.put("textDomain.commentNum", lastCommentNum);
		return this.queryByExample(example, desc, lastKeyValue, pageSize);
	}

}

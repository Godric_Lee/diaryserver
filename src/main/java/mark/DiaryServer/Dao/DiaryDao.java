package mark.DiaryServer.Dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.Diary;
import mark.DiaryServer.Domain.TextDomain;

@Repository
public class DiaryDao extends BaseDao<Diary> {

	@Override
	public String save(Diary diary) {
		TextDomain textDomain = diary.getTextDomain();
		textDomain = new TextDomain(textDomain.getTextContent(), textDomain.getFromUser(), 0, 0,
				textDomain.getAnonymousStatus(), new Date(), diary.getTextDomain().getDomainRemark());

		diary = new Diary(diary.getDiaryWeather(), diary.getDiaryMood(), diary.getDiaryTitle(), textDomain,
				diary.getPictureDiary());
		return super.save(diary);
	}

	/**
	 * 更新diary实例
	 * 
	 * @param diary
	 * @param user
	 * @throws Exception
	 */
	@Override
	public void update(Diary diary) {
		Diary originalDiary = this.load(diary.getId());
		Diary newDiary = diary;

		newDiary.getTextDomain().setDigestNum(originalDiary.getTextDomain().getDigestNum());
		newDiary.getTextDomain().setCommentNum(originalDiary.getTextDomain().getCommentNum());
		newDiary.getTextDomain().setFromUser(originalDiary.getTextDomain().getFromUser());

		this.update(newDiary);
	}

	/**
	 * 获取Diaries 按时间排序
	 * 
	 * @param desc
	 * @param lastDate
	 * @param pageSize
	 * @param pictureDiary
	 * @return
	 */
	public List<Diary> orderByCreateTime(boolean desc, Date lastDate, int pageSize, boolean pictureDiary) {
		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("textDomain.createTime", lastDate);
		Diary example = new Diary();
		example.setPictureDiary(pictureDiary);
		return this.queryByExample(example, desc, lastKeyValue, pageSize);
	}

	/**
	 * 获取所有Diary 按热度降续
	 * 
	 * @param desc
	 * @param lastDigestNum
	 * @param lastCommentNum
	 * @param pageSize
	 * @param pictureDiary
	 * @return
	 */
	public List<Diary> orderByHotNum(boolean desc, int pageNo, int pageSize, boolean pictureDiary) {

		List<String> lastKey = new ArrayList<>();
		lastKey.add("textDomain.digestNum");
		lastKey.add("textDomain.commentNum");
		Diary example = new Diary();
		example.setPictureDiary(pictureDiary);
		return this.queryByExample(example, desc, lastKey, pageNo, pageSize);
	}

}

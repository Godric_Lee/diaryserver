package mark.DiaryServer.Dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.DiaryDigest;
import mark.DiaryServer.Domain.TextDomain;
import mark.DiaryServer.Domain.User;

@Repository
public class DiaryDigestDao extends BaseDao<DiaryDigest> {

	/**
	 * 获取TextId的所有点赞
	 * 
	 * @param id
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<DiaryDigest> orderByTime(TextDomain textDomain, Date lastDate, int pageSize) {
		DiaryDigest diaryDigest = new DiaryDigest();
		diaryDigest.setTextDomain(textDomain);
		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("createTime", lastDate);
		return this.queryByExample(diaryDigest, true, lastKeyValue, pageSize);
	}

	/**
	 * 取消点赞
	 * 
	 * @param id
	 * @param user
	 */
	public void cancelDigest(TextDomain textDomain, User user) {
		DiaryDigest diaryDigest = new DiaryDigest();
		diaryDigest.setTextDomain(textDomain);
		diaryDigest.setFromUser(user);
		List<DiaryDigest> list = this.queryByExample(diaryDigest, true, null, 0);
		for (DiaryDigest item : list) {
			this.delete(item);
		}

	}

	/**
	 * 用户是否点赞过
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	public boolean ifDigest(TextDomain textDomain, User user) {
		DiaryDigest diaryDigest = new DiaryDigest();
		diaryDigest.setTextDomain(textDomain);
		diaryDigest.setFromUser(user);
		return this.queryByExample(diaryDigest, true, null, 0).size() > 0;
	}

	/**
	 * 桉指定顺序获取用户点赞记录
	 * 
	 * @param user
	 * @param orderKey
	 * @param order
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<DiaryDigest> getByUserOrderByTime(User user, Date lastDate, int pageSize) {
		DiaryDigest diaryDigest = new DiaryDigest();
		diaryDigest.getTextDomain().setFromUser(user);
		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("createTime", lastDate);
		return this.queryByExample(diaryDigest, true, lastKeyValue, pageSize);
	}

}

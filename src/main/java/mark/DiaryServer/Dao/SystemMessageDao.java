package mark.DiaryServer.Dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.SystemMessage;
import mark.DiaryServer.Domain.User;

@Repository
public class SystemMessageDao extends BaseDao<SystemMessage> {

	/**
	 * 获取用户收到的所有信息，按时间排序
	 * 
	 * @param user
	 * @param date
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<SystemMessage> getByReceiverOrderTime(User receiverUser, Date lastDate, int pageSize) {
		SystemMessage systemMessage = new SystemMessage();
		systemMessage.setReceiveUser(receiverUser);
		HashMap<String, Object> lastKeyValue = new HashMap<>();
		lastKeyValue.put("createTime", lastDate);
		return this.queryByExample(systemMessage, true, lastKeyValue, pageSize);

	}

	/**
	 * 获取用户收到的消息 分页查询
	 * 
	 * @param receiverUser
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<SystemMessage> pageQueryByUser(User receiverUser, int pageNo, int pageSize) {
		SystemMessage systemMessage = new SystemMessage();
		systemMessage.setReceiveUser(receiverUser);
		List<String> args = new ArrayList<String>();
		args.add("createTime");
		return this.queryByExample(systemMessage, true, args, pageNo, pageSize);
	}

	/**
	 * 设置消息为已读
	 * 
	 * @param user
	 * @param id
	 */
	public void setCheckMessage(SystemMessage systemMessage, User user) {
		SystemMessage example = new SystemMessage();
		example.setDomainUuid(systemMessage.getDomainUuid());
		example.setReceiveUser(user);
		List<SystemMessage> list = this.queryByExample(example, true, null, 0);
		for (SystemMessage item : list) {
			item.setIfCheck(true);
			this.update(item);
		}
	}

}

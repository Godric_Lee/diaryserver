package mark.DiaryServer.Dao;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.TextDomain;

@Repository
public class TextDomainDao extends BaseDao<TextDomain> {

	/**
	 * 点赞数加1
	 * 
	 * @param textDomain
	 */
	public void incDigestNum(TextDomain textDomain) {
		textDomain.setDigestNum(textDomain.getDigestNum() + 1);
		this.update(textDomain);
	}

	/**
	 * 点赞数减1
	 * 
	 * @param textDomain
	 */
	public void decDigestNum(TextDomain textDomain) {
		textDomain.setDigestNum(textDomain.getDigestNum() - 1);
		this.update(textDomain);
	}

	/**
	 * 评论数加1
	 * 
	 * @param textDomain
	 */
	public void incCommentNum(TextDomain textDomain) {
		textDomain = this.load(textDomain.getDomainUuid());
		System.out.println("----------" + textDomain.getCommentNum());
		textDomain.setCommentNum(textDomain.getCommentNum() == null ? 0 : textDomain.getCommentNum() + 1);
		this.update(textDomain);
	}

	/**
	 * 评论数减1
	 * 
	 * @param textDomain
	 */
	public void decCommentNum(TextDomain textDomain) {
		textDomain.setCommentNum(textDomain.getCommentNum() + 1);
		this.update(textDomain);
	}

	/**
	 * 设置匿名状态
	 * 
	 * @param textDomain
	 * @param ifAnonymous
	 */
	public void setAnonymousStatus(TextDomain textDomain, Boolean ifAnonymous) {
		textDomain.setAnonymousStatus(ifAnonymous);
		this.update(textDomain);
	}
}

package mark.DiaryServer.Dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.User;

@Repository
public class UserDao extends BaseDao<User> {

	/**
	 * 手机号登录
	 * 
	 * @param user
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	public User getUserByPhone(User user) throws NoSuchFieldException, SecurityException {
		User example = new User();
		example.setUserPhone(user.getUserPhone());
		example.setPassWord(user.getPassWord());

		List<User> list = this.queryByExample(example, true, null, 1);
		return list.size() > 0 ? list.get(0) : null;
	}

	/**
	 * 更新用户信息
	 * 
	 * @param user
	 * @param newUser
	 */
	public void updateInfo(User user, User newUser) {
		if (newUser.getUserName() != null)
			user.setUserName(newUser.getUserName());
		if (newUser.getUserHead() != null)
			user.setUserHead(newUser.getUserHead());
		if (newUser.getUserContent() != null)
			user.setUserContent(newUser.getUserContent());
		if (newUser.getUserGender() != null)
			user.setUserGender(newUser.getUserGender());
		this.update(user);
	}

}

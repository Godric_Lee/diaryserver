package mark.DiaryServer.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserRelation;

@Repository
public class UserRelationDao extends BaseDao<UserRelation> {

	/**
	 * 获取所有关注者
	 * 
	 * @param user
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<UserRelation> getFollowers(User user, int pageNo, int pageSize) {
		UserRelation userRelation = new UserRelation();
		userRelation.setToUser(user);
		List<String> orderKey = new ArrayList<String>();
		orderKey.add("createTime");
		return this.queryByExample(userRelation, true, orderKey, pageNo, pageSize);
	}

	/**
	 * 获取所有关注的人
	 * 
	 * @param user
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<UserRelation> getFollows(User user, int pageNo, int pageSize) {
		UserRelation userRelation = new UserRelation();
		userRelation.setFromUser(user);
		List<String> orderKey = new ArrayList<String>();
		orderKey.add("createTime");
		return this.queryByExample(userRelation, true, orderKey, pageNo, pageSize);
	}

	/**
	 * 获取用户A是否关注用户B
	 * 
	 * @param fromUser
	 * @param toUser
	 * @return
	 */
	public boolean ifFollow(User fromUser, User toUser) {

		return this.getItem(fromUser, toUser).size() > 0;
	}

	/**
	 * 获取用户A关注用户B的记录
	 * 
	 * @param fromUser
	 * @param toUser
	 * @return
	 */
	public List<UserRelation> getItem(User fromUser, User toUser) {
		UserRelation userRelation = new UserRelation();
		userRelation.setFromUser(fromUser);
		userRelation.setToUser(toUser);
		return this.queryByExample(userRelation, true, null, 0);
	}

}

package mark.DiaryServer.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserToken;

@Repository
public class UserTokenDao extends BaseDao<UserToken> {

	/**
	 * 获取用户的token 不存在则创建
	 * 
	 * @param user
	 * @return
	 */
	public UserToken getToken(User user) {
		UserToken example = new UserToken();
		example.setUser(user);
		try {
			List<UserToken> list = this.queryByExample(example, true, null, 1);
			if (list.size() == 0) {
				throw new Exception();
			}
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			UserToken token = new UserToken(user, new Date(), 120);
			String id = this.save(token);
			return this.get(id);
		}

	}

	/**
	 * 根据Token获取User
	 * 
	 * @param token
	 * @return
	 */
	public User getUser(String token) {
		UserToken userToken = null;
		try {
			userToken = this.load(token);
			if (((new Date()).getDay() - userToken.getCreateTime().getDay() >= 0)
					&& ((new Date()).getDay() - userToken.getCreateTime().getDay() < 30)) {
				this.updateTime(userToken);
				return userToken.getUser();
			} else {
				this.delete(userToken);
				throw new Exception();
			}
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 更新token创建时间
	 * 
	 * @param userToken
	 */
	public void updateTime(UserToken userToken) {
		userToken.setCreateTime(new Date());
		this.update(userToken);
	}
}

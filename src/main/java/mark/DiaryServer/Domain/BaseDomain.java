package mark.DiaryServer.Domain;

import java.io.Serializable;
import java.util.Date;

//基础类
public class BaseDomain implements Serializable {

	private String domainUuid;

	private Date createTime;
	// 备注(冗余)
	private String domainRemark;

	public BaseDomain() {

	}

	public BaseDomain(Date createTime, String domainRemark) {
		this.createTime = createTime;
		this.domainRemark = domainRemark;
	}

	public BaseDomain(String domainUuid, Date createTime, String domainRemark) {
		this.domainUuid = domainUuid;
		this.createTime = createTime;
		this.domainRemark = domainRemark;
	}

	public BaseDomain(BaseDomain baseDomain) {
		this(baseDomain.getDomainUuid(), baseDomain.getCreateTime(), baseDomain.getDomainRemark());
	}

	public String getDomainUuid() {
		return domainUuid;
	}

	public void setDomainUuid(String domainUuid) {
		this.domainUuid = domainUuid;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getDomainRemark() {
		return this.domainRemark;
	}

	public void setDomainRemark(String domainRemark) {
		this.domainRemark = domainRemark;
	}
}

package mark.DiaryServer.Domain;

import java.io.Serializable;
import java.util.List;

//日记实体类
public class Diary implements Serializable {

	private String id;

	private String diaryWeather;

	private String diaryMood;

	private String diaryTitle;
	// TextDomian 主体
	private TextDomain textDomain;
	// 是否以图片为主体
	private Boolean pictureDiary;
	// 评论 只在运行时使用
	private List<DiaryComment> diaryComments;

	public Diary() {
		this.textDomain = new TextDomain();
	}

	public Diary(String diaryWeather, String diaryMood, String diaryTitle, TextDomain textDomain,
			Boolean pictureDiary) {
		this.diaryWeather = diaryWeather;
		this.diaryMood = diaryMood;
		this.diaryTitle = diaryTitle;
		this.textDomain = textDomain;
		this.pictureDiary = pictureDiary;
	}

	public Diary(Diary diary) {
		this(diary.getDiaryWeather(), diary.getDiaryMood(), diary.getDiaryTitle(),
				new TextDomain(diary.getTextDomain()), diary.getPictureDiary());

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDiaryWeather() {
		return diaryWeather;
	}

	public void setDiaryWeather(String diaryWeather) {
		this.diaryWeather = diaryWeather;
	}

	public String getDiaryMood() {
		return diaryMood;
	}

	public void setDiaryMood(String diaryMood) {
		this.diaryMood = diaryMood;
	}

	public String getDiaryTitle() {
		return diaryTitle;
	}

	public void setDiaryTitle(String diaryTitle) {
		this.diaryTitle = diaryTitle;
	}

	public TextDomain getTextDomain() {
		return textDomain;
	}

	public void setTextDomain(TextDomain textDomain) {
		this.textDomain = textDomain;
	}

	public Boolean getPictureDiary() {
		return pictureDiary;
	}

	public void setPictureDiary(Boolean pictureDiary) {
		this.pictureDiary = pictureDiary;
	}

	public List<DiaryComment> getDiaryComments() {
		return diaryComments;
	}

	public void setDiaryComments(List<DiaryComment> diaryComments) {
		this.diaryComments = diaryComments;
	}

}

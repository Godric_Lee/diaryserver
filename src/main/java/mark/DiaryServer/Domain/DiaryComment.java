package mark.DiaryServer.Domain;

import java.io.Serializable;

//信件的评论的实体类
public class DiaryComment implements Serializable {

	private String id;
	// 上一条文本
	private TextDomain lastComment;
	// 文本主体
	private TextDomain textDomain;

	public DiaryComment() {

	}

	public DiaryComment(TextDomain lastComment, TextDomain textDomain) {
		this.lastComment = lastComment;
		this.textDomain = textDomain;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TextDomain getLastComment() {
		return lastComment;
	}

	public void setLastComment(TextDomain lastComment) {
		this.lastComment = lastComment;
	}

	public TextDomain getTextDomain() {
		return textDomain;
	}

	public void setTextDomain(TextDomain textDomain) {
		this.textDomain = textDomain;
	}

}

package mark.DiaryServer.Domain;

//日记的赞的实体
public class DiaryDigest extends BaseDomain {

	private User fromUser;

	private TextDomain textDomain;

	public DiaryDigest() {
		super();
	}

	public DiaryDigest(User fromUser, TextDomain textDomain) {
		super();
		this.fromUser = fromUser;
		this.textDomain = textDomain;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public TextDomain getTextDomain() {
		return textDomain;
	}

	public void setTextDomain(TextDomain textDomain) {
		this.textDomain = textDomain;
	}

}

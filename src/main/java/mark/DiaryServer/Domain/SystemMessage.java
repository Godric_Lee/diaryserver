package mark.DiaryServer.Domain;

//发给用户的消息
public class SystemMessage extends BaseDomain {

	private User fromUser;

	private User receiveUser;

	private Class domainClass;

	private String domainId;

	private Boolean ifCheck;

	public SystemMessage() {
		super();
		this.fromUser = new User();
		this.receiveUser = new User();
	}

	public SystemMessage(User fromUser, User receiveUser, Class domainClass, String domainId, Boolean ifCheck) {
		super();
		this.fromUser = fromUser;
		this.receiveUser = receiveUser;
		this.domainClass = domainClass;
		this.domainId = domainId;
		this.ifCheck = ifCheck;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getReceiveUser() {
		return receiveUser;
	}

	public void setReceiveUser(User receiveUser) {
		this.receiveUser = receiveUser;
	}

	public Class getDomainClass() {
		return domainClass;
	}

	public void setDomainClass(Class domainClass) {
		this.domainClass = domainClass;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public Boolean getIfCheck() {
		return ifCheck;
	}

	public void setIfCheck(Boolean ifCheck) {
		this.ifCheck = ifCheck;
	}

}

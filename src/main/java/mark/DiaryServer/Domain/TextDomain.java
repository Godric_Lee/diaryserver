package mark.DiaryServer.Domain;

import java.util.Date;

//文字相关的类
public class TextDomain extends BaseDomain {
	// 内容
	private String textContent;

	private User fromUser;

	private Integer digestNum;

	private Integer commentNum;
	// 匿名状态
	private Boolean anonymousStatus;

	public TextDomain() {
		super();
		this.fromUser = new User();
	}

	public TextDomain(String textContent, User fromUser, Integer digestNum, Integer commentNum, Boolean anonymousStatus,
			Date createTime, String domainRemark) {
		super(createTime, domainRemark);
		this.textContent = textContent;
		this.fromUser = fromUser;
		this.digestNum = digestNum;
		this.commentNum = commentNum;
		this.anonymousStatus = anonymousStatus;
	}

	public TextDomain(TextDomain textDomain) {
		this(textDomain.getTextContent(), textDomain.getFromUser(), textDomain.getDigestNum(),
				textDomain.getCommentNum(), textDomain.getAnonymousStatus(), textDomain.getCreateTime(),
				textDomain.getDomainRemark());
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public Integer getDigestNum() {
		return digestNum;
	}

	public void setDigestNum(Integer digestNum) {
		this.digestNum = digestNum;
	}

	public Integer getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	public Boolean getAnonymousStatus() {
		return anonymousStatus;
	}

	public void setAnonymousStatus(Boolean anonymousStatus) {
		this.anonymousStatus = anonymousStatus;
	}

}

package mark.DiaryServer.Domain;

//用户
public class User extends BaseDomain {

	private String userName;

	private String passWord;

	private String userPhone;

	private String userEmail;
	// 性别
	private Integer userGender;
	// 个人介绍
	private String userComment;
	// 个性签名
	private String userContent;
	// 头像所在位置
	private String userHead;
	// 用户封锁状态
	private Boolean userStatus;

	private Integer followerNum;

	private Integer followNum;

	public Integer getFollowNum() {
		return followNum;
	}

	public void setFollowNum(Integer followNum) {
		this.followNum = followNum;
	}

	public User() {
		super();
	}

	public User(String userName, String passWord, String userPhone, String userEmail, String userComment,
			String userContent, String userHead, Boolean userStatus) {
		super();
		this.userName = userName;
		this.passWord = passWord;
		this.userPhone = userPhone;
		this.userEmail = userEmail;
		this.userComment = userComment;
		this.userContent = userContent;
		this.userHead = userHead;
		this.userStatus = userStatus;
	}

	public User(User user) {
		this(user.getUserName(), user.getPassWord(), user.getUserPhone(), user.getUserEmail(), user.getUserComment(),
				user.getUserContent(), user.getUserHead(), user.getUserStatus());
		this.setDomainUuid(user.getDomainUuid());
		this.setCreateTime(user.getCreateTime());
		this.setDomainRemark(user.getDomainRemark());
	}

	public Boolean equals(User user) {
		return this.getDomainUuid().equals(user.getDomainUuid());
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String password) {
		this.passWord = password;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Integer getUserGender() {
		return userGender;
	}

	public void setUserGender(Integer userGender) {
		this.userGender = userGender;
	}

	public String getUserComment() {
		return userComment;
	}

	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	public String getUserContent() {
		return userContent;
	}

	public void setUserContent(String userContent) {
		this.userContent = userContent;
	}

	public String getUserHead() {
		return userHead;
	}

	public void setUserHead(String userHead) {
		this.userHead = userHead;
	}

	public Boolean getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Boolean userStatus) {
		this.userStatus = userStatus;
	}

	public Integer getFollowerNum() {
		return followerNum;
	}

	public void setFollowerNum(Integer followerNum) {
		this.followerNum = followerNum;
	}

}

package mark.DiaryServer.Domain;

//用户关注关系的实体类
public class UserRelation extends BaseDomain {

	private User fromUser;

	private User toUser;

	public UserRelation() {

	}

	public UserRelation(User fromUser, User toUser) {
		super();
		this.fromUser = fromUser;
		this.toUser = toUser;
	}

	public UserRelation(UserRelation userRelation) {
		this(userRelation.getFromUser(), userRelation.getToUser());
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getToUser() {
		return toUser;
	}

	public void setToUser(User toUser) {
		this.toUser = toUser;
	}

}

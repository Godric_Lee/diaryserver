package mark.DiaryServer.Domain;

import java.util.Date;

//用户token
public class UserToken {

	private User user;

	private String token;

	private Date createTime;

	private Integer aliveTime;

	public UserToken() {

	}

	public UserToken(User user, Date createTime, Integer aliveTime) {
		this.user = user;
		this.createTime = createTime;
		this.aliveTime = aliveTime;
	}

	public UserToken(UserToken userToken) {
		this(userToken.getUser(), userToken.getCreateTime(), userToken.getAliveTime());
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getAliveTime() {
		return aliveTime;
	}

	public void setAliveTime(Integer aliveTime) {
		this.aliveTime = aliveTime;
	}

}

package mark.DiaryServer.Service;

import org.springframework.beans.factory.annotation.Autowired;

import mark.DiaryServer.Dao.UserTokenDao;
import mark.DiaryServer.Domain.User;

public class BaseService {
	@Autowired
	protected UserTokenDao userTokenDao;

	public User getUser(String token) {
		return this.userTokenDao.getUser(token);
	}
}

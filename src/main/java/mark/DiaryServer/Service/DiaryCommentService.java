package mark.DiaryServer.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Dao.DiaryCommentDao;
import mark.DiaryServer.Dao.TextDomainDao;
import mark.DiaryServer.Domain.DiaryComment;
import mark.DiaryServer.Domain.User;

@Service
public class DiaryCommentService extends BaseService {

	@Autowired
	private DiaryCommentDao diaryCommentDao;

	@Autowired
	private TextDomainDao textDomainDao;

	/**
	 * 添加新评论
	 * 
	 * @param diaryComment
	 * @param user
	 * @return
	 */
	public DiaryComment addDiaryComment(DiaryComment diaryComment, User user) {
		System.out.println(user.getUserPhone());
		diaryComment.getTextDomain().setFromUser(user);
		diaryComment.setLastComment(this.textDomainDao.get(diaryComment.getLastComment().getDomainUuid()));

		String id = this.diaryCommentDao.save(diaryComment);
		this.textDomainDao.incCommentNum(diaryComment.getLastComment());
		return this.diaryCommentDao.load(id);
	}

	/**
	 * 删除评论
	 * 
	 * @param diaryComment
	 * @param user
	 */
	public void deleteDiaryComment(String diaryCommentId, User user) {
		DiaryComment diaryComment = this.diaryCommentDao.load(diaryCommentId);
		if (!diaryComment.getTextDomain().getFromUser().equals(user))
			return;
		this.textDomainDao.decCommentNum(diaryComment.getLastComment());
		this.diaryCommentDao.delete(diaryComment);
	}

	/**
	 * 评论详细
	 * 
	 * @param diaryCommentId
	 * @return
	 */
	public DiaryComment getDiaryCommentById(String diaryCommentId) {
		return this.diaryCommentDao.get(diaryCommentId);
	}

	/**
	 * 获取用户发出的所有评论
	 * 
	 * @param user
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<DiaryComment> getDiaryCommentsByUser(User user, Date lastDate, int pageSize) {
		return this.diaryCommentDao.getByFromUserOrderByTime(user, true, lastDate, pageSize, null);
	}

	/**
	 * 获取指定实体的所有评论，按照时间降续排序
	 * 
	 * @param id
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<DiaryComment> getDiaryCommentsOrderByTime(String lastId, Date lastDate, int pageSize) {
		return this.diaryCommentDao.orderByCreateTime(lastId, true, lastDate, pageSize);
	}

	/**
	 * 获取指定实体的所有评论 按照热度降序排序
	 * 
	 * @param id
	 * @param lastDigestNum
	 * @param lastCommentNum
	 * @param pageSize
	 * @return
	 */
	public List<DiaryComment> getDiaryCommentsOrderByHotNum(String lastId, int lastDigestNum, int lastCommentNum,
			int pageSize) {
		return this.diaryCommentDao.orderByHotNum(lastId, true, lastDigestNum, lastCommentNum, pageSize);
	}
}

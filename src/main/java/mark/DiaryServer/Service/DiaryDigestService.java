package mark.DiaryServer.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Config.CustomException;
import mark.DiaryServer.Dao.DiaryDigestDao;
import mark.DiaryServer.Dao.TextDomainDao;
import mark.DiaryServer.Domain.DiaryDigest;
import mark.DiaryServer.Domain.TextDomain;
import mark.DiaryServer.Domain.User;

@Service
public class DiaryDigestService extends BaseService {

	@Autowired
	private DiaryDigestDao diaryDigestDao;

	@Autowired
	private TextDomainDao textDomainDao;

	/**
	 * 获取用户所有点赞的记录
	 * 
	 * @param userId
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<DiaryDigest> getDigestsByUser(String userId, Date lastDate, int pageSize) {
		User user = getUser(userId);
		return this.diaryDigestDao.getByUserOrderByTime(user, lastDate, pageSize);
	}

	/**
	 * 用户是否已经点赞
	 * 
	 * @param textDomainId
	 * @param user
	 * @return
	 */
	public boolean ifDigest(String textDomainId, User user) {

		TextDomain textDomain = this.textDomainDao.load(textDomainId);

		return this.diaryDigestDao.ifDigest(textDomain, user);
	}

	/**
	 * 点赞
	 * 
	 * @param diaryDigest
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public DiaryDigest addDigest(DiaryDigest diaryDigest, User user) throws Exception {

		diaryDigest.setFromUser(user);
		TextDomain textDomain = this.textDomainDao.load(diaryDigest.getTextDomain().getDomainUuid());
		if (this.diaryDigestDao.ifDigest(textDomain, user)) {
			throw new Exception("已赞同");
		}

		String id = this.diaryDigestDao.save(diaryDigest);
		textDomainDao.incDigestNum(textDomain);

		return this.diaryDigestDao.load(id);

	}

	/**
	 * 取消点赞
	 * 
	 * @param diaryDigestId
	 * @param user
	 * @throws CustomException
	 */
	public boolean cancelDigest(String textDomainId, User user) throws CustomException {

		TextDomain textDomain = this.textDomainDao.load(textDomainId);
		this.diaryDigestDao.cancelDigest(textDomain, user);

		this.textDomainDao.decDigestNum(textDomain);
		return !diaryDigestDao.ifDigest(textDomain, user);
	}
}

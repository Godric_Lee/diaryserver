package mark.DiaryServer.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Dao.DiaryDao;
import mark.DiaryServer.Dao.TextDomainDao;
import mark.DiaryServer.Dao.UserDao;
import mark.DiaryServer.Domain.Diary;
import mark.DiaryServer.Domain.User;

@Service
public class DiaryService extends BaseService {

	@Autowired
	private DiaryDao diaryDao;

	@Autowired
	private TextDomainDao textDomainDao;

	@Autowired
	private UserDao userDao;

	/**
	 * 添加新日记
	 * 
	 * @param diary
	 * @param user
	 * @return
	 */
	public Diary addDiary(Diary diary, User user) {

		diary.getTextDomain().setFromUser(user);
		String id = this.diaryDao.save(diary);

		return this.diaryDao.load(id);
	}

	/**
	 * 更新日记
	 * 
	 * @param diary
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public Diary updateDiary(Diary newDiary, User user) throws Exception {
		Diary diary = this.diaryDao.load(newDiary.getId());
		if (!diary.getTextDomain().getFromUser().equals(user)) {
			return null;
		}
		this.diaryDao.update(newDiary);
		return this.diaryDao.load(diary.getId());

	}

	/**
	 * 信件详细
	 * 
	 * @param diaryId
	 * @return
	 */
	public Diary getDiaryById(String diaryId) {
		return this.diaryDao.get(diaryId);
	}

	/**
	 * 获取用户的所有发出的信件
	 * 
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<Diary> getDiariesByUser(String userId, Date lastDate, int pageSize, Boolean pictureDiary) {
		User user = this.userDao.load(userId);

		return this.diaryDao.getByFromUserOrderByTime(user, true, lastDate, pageSize, pictureDiary);
	}

	/**
	 * 获取所有的Diaries 按创建时间降续排序
	 * 
	 * @param lastDate
	 * @param pageSize
	 * @param pictureDiary
	 * @return
	 */
	public List<Diary> getDiariesOrderByTime(Date lastDate, int pageSize, boolean pictureDiary) {
		return this.diaryDao.orderByCreateTime(true, lastDate, pageSize, pictureDiary);
	}

	/**
	 * 获取所有的Diaries 按热度降序排序
	 * 
	 * @param lastDigestNum
	 * @param lastCommentNum
	 * @param pageSize
	 * @param pictureDiary
	 * @return
	 */
	public List<Diary> getDiariesOrderByHotNum(int pageNo, int pageSize, boolean pictureDiary) {
		return this.diaryDao.orderByHotNum(true, pageNo, pageSize, pictureDiary);
	}

}

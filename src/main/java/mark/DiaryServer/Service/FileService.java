package mark.DiaryServer.Service;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import mark.DiaryServer.Domain.User;

@Service
public class FileService {

	public enum PictureType {
		DIARY_PICTURE, HEAD_PICTURE
	}

	public String addDiaryPicture(MultipartFile file, String name, User user, HttpServletRequest request) {
		return this.addPicture(file, name, user, request, PictureType.DIARY_PICTURE);
	}

	public String addHeadPicture(MultipartFile file, String name, User user, HttpServletRequest request) {
		return this.addPicture(file, name, user, request, PictureType.HEAD_PICTURE);
	}

	private String addPicture(MultipartFile file, String name, User user, HttpServletRequest request,
			PictureType type) {
		if (user == null) {
			return null;
		}
		String path = request.getSession().getServletContext().getRealPath("/");
		String temp = path;
		switch (type) {
		case DIARY_PICTURE:
			temp += "diaryPictures";
			break;
		case HEAD_PICTURE:
			temp += "headPictures";
			break;
		}

		File newFile = new File(temp);
		String filename = "";
		if (!newFile.exists()) {
			newFile.mkdir();
		}

		if (!file.isEmpty()) {
			try {

				switch (type) {
				case DIARY_PICTURE:
					filename = "diaryPictures/" + (new Date()).getTime() + "-" + file.getOriginalFilename();
					break;
				case HEAD_PICTURE:
					filename = "headPictures/" + (new Date()).getTime() + "-" + file.getOriginalFilename();
					break;
				}

				newFile = new File(path + filename);
				newFile.setExecutable(false);
				file.transferTo(newFile);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return filename;
	}
}

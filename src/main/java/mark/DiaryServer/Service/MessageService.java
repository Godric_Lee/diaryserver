package mark.DiaryServer.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Dao.SystemMessageDao;
import mark.DiaryServer.Domain.SystemMessage;
import mark.DiaryServer.Domain.User;

@Service
public class MessageService extends BaseService {

	@Autowired
	private SystemMessageDao systemMessageDao;

	/**
	 * 保存消息
	 * 
	 * @param systemMessage
	 * @return
	 */
	public SystemMessage addSystemMessage(SystemMessage systemMessage) {
		String id = this.systemMessageDao.save(systemMessage);
		return this.systemMessageDao.load(id);
	}

	/**
	 * 获取用户所有收到的系统消息
	 * 
	 * @param user
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<SystemMessage> getSystemMessagesByReceiver(User user, Date lastDate, int pageSize) {

		return this.systemMessageDao.getByReceiverOrderTime(user, lastDate, pageSize);

	}

	/**
	 * 获取用户所有收到的系统消息
	 * 
	 * @param user
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<SystemMessage> getSystemMessagesByReceiver(User user, int pageNo, int pageSize) {

		return this.systemMessageDao.pageQueryByUser(user, pageNo, pageSize);

	}

	/**
	 * 设置消息已读
	 * 
	 * @param user
	 * @param message
	 */
	public void setCheckSystemMessage(User user, String messageId) {
		SystemMessage systemMessage = this.systemMessageDao.load(messageId);
		this.systemMessageDao.setCheckMessage(systemMessage, user);
	}

}

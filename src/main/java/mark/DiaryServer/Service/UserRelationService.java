package mark.DiaryServer.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Config.CustomException;
import mark.DiaryServer.Dao.UserDao;
import mark.DiaryServer.Dao.UserRelationDao;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserRelation;

@Service
public class UserRelationService extends BaseService {

	@Autowired
	private UserRelationDao userRelationDao;

	@Autowired
	private UserDao userDao;

	/**
	 * 关注用户
	 * 
	 * @param userRelation
	 * @param fromUser
	 * @return
	 * @throws CustomException
	 */
	public UserRelation follow(UserRelation userRelation, User fromUser) throws CustomException {
		if (this.userRelationDao.ifFollow(fromUser, userRelation.getToUser())) {
			throw new CustomException("已关注");
		}
		userRelation.setFromUser(fromUser);
		String id = userRelationDao.save(userRelation);
		return this.userRelationDao.get(id);
	}

	/**
	 * 取消关注
	 * 
	 * @param fromUser
	 * @param toUser
	 */
	public void unfollow(User fromUser, User toUser) {
		List<UserRelation> list = this.userRelationDao.getItem(fromUser, toUser);
		for (UserRelation item : list) {
			this.userRelationDao.delete(item);
		}
	}

	/**
	 * 获取所有followers
	 * 
	 * @param user
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<UserRelation> getFollowers(User user, int pageNo, int pageSize) {

		return this.userRelationDao.getFollowers(user, pageNo, pageSize);

	}

	/**
	 * 获取所有follow
	 * 
	 * @param user
	 * @param lastDate
	 * @param pageSize
	 * @return
	 */
	public List<UserRelation> getFollows(User user, int pageNo, int pageSize) {

		return this.userRelationDao.getFollows(user, pageNo, pageSize);
	}
}

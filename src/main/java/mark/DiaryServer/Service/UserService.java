package mark.DiaryServer.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import mark.DiaryServer.Config.CustomException;
import mark.DiaryServer.Dao.UserDao;
import mark.DiaryServer.Dao.UserTokenDao;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserToken;

@Service
public class UserService extends BaseService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserTokenDao userTokenDao;

	public UserToken getUserToken(String token) {
		return this.userTokenDao.get(token);
	}

	/**
	 * 根据用户id获取用户
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserById(String userId) {
		return this.userDao.get(userId);
	}

	/**
	 * 通过token获取用户
	 * 
	 * @param token
	 * @return
	 */
	public User getUserFromToken(String token) {
		return this.userTokenDao.getUser(token);
	}

	/**
	 * 用户注册
	 * 
	 * @param user
	 * @return
	 */
	public User userRegister(User user) {
		String id = userDao.save(user);
		return userDao.get(id);
	}

	/**
	 * 手机号登录
	 * 
	 * @param user
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	public UserToken userLoginByPhone(User user) throws NoSuchFieldException, SecurityException {
		user = userDao.getUserByPhone(user);

		return this.userTokenDao.getToken(user);
	}

	/**
	 * 更新用户信息并返回新用户
	 * 
	 * @param user
	 * @param newUser
	 * @return
	 * @throws CustomException
	 * @throws DataAccessException
	 * @throws Exception
	 */
	public User updateInfo(User user, User newUser) throws CustomException {
		if (!user.equals(newUser)) {
			throw new CustomException("error");
		}
		this.userDao.updateInfo(user, newUser);
		return this.userDao.load(user.getDomainUuid());
	}
}

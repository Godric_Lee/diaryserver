package mark.DiaryServer.Util;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.Criteria;

public class ClassUtil {

	/**
	 * 将实例中的所有不为空的属性映射至HashMap(String,Object)
	 * 
	 * @param prefix
	 * @param entity
	 * @return
	 */
	public static <T extends Object> HashMap<String, Object> getExampleMap(String prefix, T entity, Criteria criteria) {
		if (prefix != null) {
			criteria.createAlias(prefix, prefix.replace('.', '-'));
			// System.out.println(prefix);
		}

		HashMap<String, Object> result = new HashMap<>();

		System.out.println(entity.getClass().toString());

		if (entity.getClass() == null) {
			return result;
		}
		for (Class entityClass = entity.getClass(); entityClass != null; entityClass = entityClass.getSuperclass()) {

			for (Field field : entityClass.getDeclaredFields()) {
				Type type = field.getType();
				field.setAccessible(true);

				if (type.toString().contains("java.")) {
					try {
						Object value;
						if ((value = field.get(entity)) != null) {
							result.put((prefix == null ? "" : (prefix.replace('.', '-') + ".")) + field.getName(),
									value);

						}

					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				} else {
					try {

						result.putAll(getExampleMap((prefix == null ? "" : (prefix + ".")) + field.getName(),
								field.get(entity), criteria));
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					} catch (Exception e) {
						// 此处的错误是到达边界返回null，懒得判断了
					}
				}
				field.setAccessible(false);
			}
		}
		return result;
	}

	/**
	 * 对实例中指定的变量进行赋值
	 * 
	 * @param entity
	 * @param fieldName
	 * @param fieldValue
	 * @return
	 */
	public static <T extends Object> boolean setField(T entity, String fieldName, Object fieldValue) {
		Field field = getField(entity.getClass(), fieldName);
		if (field == null)
			return false;
		field.setAccessible(true);
		try {
			field.set(entity, fieldValue);
		} catch (Exception e) {
			return false;
		} finally {
			field.setAccessible(false);
		}
		return true;
	}

	/**
	 * 获取类中指定的Field
	 * 
	 * @param clazz
	 * @param name
	 * @return
	 */
	public static Field getField(Class clazz, String name) {
		if (clazz == null)
			return null;

		try {
			return clazz.getDeclaredField(name);
		} catch (NoSuchFieldException e) {
			return getField(clazz.getSuperclass(), name);
		}

	}

	/**
	 * 获取类中指定的Fields
	 * 
	 * @param clazz
	 * @param names
	 * @return
	 */
	public static Field[] getFields(Class clazz, String[] names) {
		ArrayList<Field> result = new ArrayList<>();
		for (String str : names) {
			result.add(getField(clazz, str));
		}
		return (Field[]) result.toArray();
	}

	/**
	 * 对originalObject实例进行更新，使用newObject中的值代替originalObject中的值
	 * 
	 * @param originalObject
	 * @param newObject
	 * @throws Exception
	 */
	public static void updateObject(Object originalObject, Object newObject) throws Exception {
		if (originalObject.getClass() == null) {
			return;
		}
		if (!originalObject.getClass().toString().equals(newObject.getClass().toString())) {
			System.out.println("updateObject error!");
			throw new Exception();
		}
		Class originalClass = originalObject.getClass();
		do {
			Field[] fields = originalClass.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				String clazzString = field.getClass().toString();

				Object originalValue = field.get(originalObject);
				Object newValue = field.get(newObject);

				// 基本类型
				if (clazzString.contains("java")) {
					if (newValue != null && !newValue.equals(originalObject)) {
						field.set(originalObject, newValue);
					}
				} else {
					// 实例
					updateObject(originalValue, newValue);
				}
				field.setAccessible(false);

			}
		} while ((originalClass = originalClass.getSuperclass()) != null);
	}

	/**
	 * 对originalObject实例进行更新，使用newObject中在names中的值代替originalObject中的值
	 * 
	 * @param originalObject
	 * @param newObject
	 * @param names
	 * @throws Exception
	 */
	public static void updateObject(Object originalObject, Object newObject, Field[] names) throws Exception {
		if (!originalObject.getClass().toString().equals(newObject.getClass().toString())) {
			System.out.println("updateObject error!");
			throw new Exception();
		}

		ArrayList<Field> nameList = new ArrayList<>();
		for (int i = 0; i < names.length; i++)
			nameList.add(names[i]);

		Class originalClass = originalObject.getClass();
		do {
			Field[] fields = originalClass.getDeclaredFields();
			for (Field field : fields) {
				if (!nameList.contains(field))
					continue;
				field.setAccessible(true);
				String clazzString = field.getClass().toString();

				Object originalValue = field.get(originalObject);
				Object newValue = field.get(newObject);

				// 基本类型
				if (clazzString.contains("java")) {
					if (newValue != null && !newValue.equals(originalObject)) {
						field.set(originalObject, newValue);
					}
				} else {
					// 实例
					updateObject(originalValue, newValue, names);
				}
				field.setAccessible(false);

			}
		} while ((originalClass = originalClass.getSuperclass()) != null);
	}
}

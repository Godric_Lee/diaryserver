package mark.DiaryServer.Web;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Service.UserService;

public class BaseController {

	@Autowired
	private UserService userService;

	public User getUser(String token) {
		return this.userService.getUserFromToken(token);
	}

	public Object returnResult(Boolean success, Object data) {
		HashMap<String, Object> hashMap = new HashMap<>();
		hashMap.put("result", success ? "true" : "false");
		hashMap.put("data", data);
		return hashMap;
	}

}

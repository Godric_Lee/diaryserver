package mark.DiaryServer.Web;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mark.DiaryServer.Domain.DiaryComment;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Service.DiaryCommentService;

@Controller
@RequestMapping("/comment")
public class CommentController extends BaseController {

	@Autowired
	private DiaryCommentService diaryCommentService;

	@RequestMapping("/add")
	@ResponseBody
	public Object addComment(DiaryComment diaryComment, String token) {
		User user = this.getUser(token);
		DiaryComment comment = this.diaryCommentService.addDiaryComment(diaryComment, user);
		return this.returnResult(true, comment);
	}

	@RequestMapping("/delete")
	@ResponseBody
	public void deleteComment(DiaryComment diaryComment, String token) {
		User user = this.getUser(token);
		this.diaryCommentService.deleteDiaryComment(diaryComment.getTextDomain().getDomainUuid(), user);
	}

	@RequestMapping("/getCommentsOrderByTime")
	@ResponseBody
	public Object getByLastIdOrderTime(String lastId, Date lastDate, Integer pageSize) {

		if (lastDate == null)
			System.out.println("----------");
		List<DiaryComment> list = this.diaryCommentService.getDiaryCommentsOrderByTime(lastId, lastDate, pageSize);
		return this.returnResult(true, list);

	}

	@RequestMapping("/getCommentsOrderByHotNum")
	@ResponseBody
	public Object getByLastIdOrderTime(String lastId, Integer lastDigestNum, Integer lastCommentNum, Integer pageSize) {
		List<DiaryComment> list = this.diaryCommentService.getDiaryCommentsOrderByHotNum(lastId, lastDigestNum,
				lastCommentNum, pageSize);
		return this.returnResult(true, list);
	}

}

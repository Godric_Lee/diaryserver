package mark.DiaryServer.Web;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import mark.DiaryServer.Domain.Diary;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Service.DiaryService;
import mark.DiaryServer.Service.FileService;

@Controller
@RequestMapping("/diary")
public class DiaryController extends BaseController {

	@Autowired
	private DiaryService diaryService;

	@Autowired
	private FileService fileService;

	@RequestMapping(path = "/addText")
	@ResponseBody
	Diary addDiary(Diary diary, String token) {
		System.out.println("-----------" + token);
		User user = this.getUser(token);
		return diaryService.addDiary(diary, user);
	}

	@RequestMapping(path = "/addPicture")
	@ResponseBody
	public Object addPicture(MultipartFile file, String name, String token, HttpServletRequest request)
			throws Exception {
		String fileName = this.fileService.addDiaryPicture(file, name, this.getUser(token), request);
		return this.returnResult(true, fileName);

		/**
		 * if (this.getUser(token) == null) {
		 * System.out.println("+++++++++++++++++++++++++"); return null; } String path =
		 * request.getSession().getServletContext().getRealPath("/"); File newFile = new
		 * File(path + "diaryPictures"); String filename = ""; if (!newFile.exists()) {
		 * newFile.mkdir(); }
		 * 
		 * if (!file.isEmpty()) { try { filename = "diaryPictures/" + (new
		 * Date()).getTime() + "-" + file.getOriginalFilename(); newFile = new File(path
		 * + filename); newFile.setExecutable(false); file.transferTo(newFile); } catch
		 * (Exception e) { e.printStackTrace(); return this.returnResult(false,
		 * "文件上传失败"); } } return this.returnResult(true, filename);
		 **/
	}

	@RequestMapping("/getByUser")
	@ResponseBody
	public Object getDiariesByUser(String userId, Date lastDate, Integer pageSize, Boolean pictureDiary) {
		List<Diary> list = this.diaryService.getDiariesByUser(userId, lastDate, pageSize, pictureDiary);
		return this.returnResult(true, list);

	}

	@RequestMapping("/orderByTime")
	@ResponseBody
	public Object getDiariesOrderByTime(HttpServletRequest request, HttpServletResponse response, Date lastDate,
			Integer pageSize, Boolean pictureDiary) {

		return this.returnResult(true, this.diaryService.getDiariesOrderByTime(lastDate, pageSize, pictureDiary));
	}

	@RequestMapping("/orderByHotNum")
	@ResponseBody
	public Object getDiariesOrderByHotNum(Integer pageNo, int pageSize, Boolean pictureDiary) {
		List<Diary> list = this.diaryService.getDiariesOrderByHotNum(pageNo, pageSize, pictureDiary);
		return this.returnResult(true, list);
	}

}

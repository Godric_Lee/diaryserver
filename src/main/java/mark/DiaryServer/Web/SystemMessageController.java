package mark.DiaryServer.Web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mark.DiaryServer.Domain.SystemMessage;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Service.MessageService;

@Controller
@RequestMapping("/systemMessage")
public class SystemMessageController extends BaseController {

	@Autowired
	private MessageService messageService;

	@RequestMapping("/get")
	@ResponseBody
	public Object getByUserOrderByTime(String token, Integer pageNo, Integer pageSize) {
		User user = this.getUser(token);
		List<SystemMessage> list = this.messageService.getSystemMessagesByReceiver(user, pageNo, pageSize);
		return this.returnResult(true, list);
	}

	@RequestMapping("/check")
	@ResponseBody
	public Object setCheck(String token, String messageId) {
		User user = this.getUser(token);
		this.messageService.setCheckSystemMessage(user, messageId);
		return this.returnResult(true, null);
	}

}

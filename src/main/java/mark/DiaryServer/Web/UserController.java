package mark.DiaryServer.Web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import mark.DiaryServer.Config.CustomException;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserToken;
import mark.DiaryServer.Service.FileService;
import mark.DiaryServer.Service.UserService;

@RequestMapping("/user")
@Controller
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private FileService fileService;

	@RequestMapping("/register")
	@ResponseBody
	public User registerUser(User user) {
		return userService.userRegister(user);
	}

	@RequestMapping(path = "/loginByPhone")
	@ResponseBody
	public Object loginByPhone(User user) throws NoSuchFieldException, SecurityException {
		UserToken userToken = userService.userLoginByPhone(user);
		return this.returnResult(true, userToken);
	}

	@RequestMapping(path = "/loginByToken")
	@ResponseBody
	public Object loginByToken(String token) throws CustomException {
		UserToken userToken = this.userService.getUserToken(token);
		return this.returnResult(true, userToken);
	}

	@RequestMapping("/index")
	@ResponseBody
	public User index(String token, HttpServletRequest request, HttpServletResponse response) {
		try {
			return this.getUser(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping("/{userId}")
	@ResponseBody
	public Object get(@PathVariable String userId) {
		System.out.println(userId);
		User user = this.userService.getUserById(userId);
		return this.returnResult(true, user);
	}

	@RequestMapping("/updateHeadPicture")
	@ResponseBody
	public Object setHeadPicture(MultipartFile file, String name, String token, HttpServletRequest request)
			throws CustomException {
		User user = this.getUser(token);
		String fileName = this.fileService.addHeadPicture(file, name, user, request);

		User newUser = new User(user);
		newUser.setUserHead(fileName);
		newUser = this.userService.updateInfo(user, newUser);
		return this.returnResult(true, newUser);
	}

	@RequestMapping("/updateInfo")
	@ResponseBody
	public Object updateInfo(String token, User newUser) throws CustomException {
		User user = this.userService.updateInfo(this.getUser(token), newUser);
		return this.returnResult(true, user);
	}

}

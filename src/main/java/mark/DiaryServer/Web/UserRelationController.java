package mark.DiaryServer.Web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserRelation;
import mark.DiaryServer.Service.UserRelationService;

@Controller
@RequestMapping("/userRelation")
public class UserRelationController extends BaseController {

	@Autowired
	private UserRelationService userRelationService;

	/**
	 * 关注
	 * 
	 * @param token
	 * @param userRelation
	 * @return
	 */
	@RequestMapping("/follow")
	@ResponseBody
	public Object addFollow(String token, UserRelation userRelation) {
		User fromUser = this.getUser(token);
		try {
			UserRelation result = this.userRelationService.follow(userRelation, fromUser);
			return this.returnResult(true, result);
		} catch (Exception e) {
			return this.returnResult(false, null);
		}

	}

	/**
	 * 取消关注
	 * 
	 * @param token
	 * @param toUser
	 * @return
	 */
	@RequestMapping("/unfollow")
	@ResponseBody
	public Object unFollow(String token, User toUser) {
		User fromUser = this.getUser(token);
		this.userRelationService.unfollow(fromUser, toUser);
		return this.returnResult(true, null);
	}

	/**
	 * 获取所有粉丝
	 * 
	 * @param token
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/getFollowers")
	@ResponseBody
	public Object getAllFollowers(String token, Integer pageNo, Integer pageSize) {
		User user = this.getUser(token);
		List<UserRelation> list = this.userRelationService.getFollowers(user, pageNo, pageSize);
		return this.returnResult(true, list);
	}

	/**
	 * 获取所有关注的人
	 * 
	 * @param token
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/getAllFollow")
	@ResponseBody
	public Object getAllFollow(String token, Integer pageNo, Integer pageSize) {
		User user = this.getUser(token);
		List<UserRelation> list = this.userRelationService.getFollows(user, pageNo, pageSize);
		return this.returnResult(true, list);
	}

}

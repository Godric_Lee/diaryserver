package mark.DiaryServer.aop;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import mark.DiaryServer.Domain.DiaryComment;
import mark.DiaryServer.Domain.DiaryDigest;
import mark.DiaryServer.Domain.SystemMessage;
import mark.DiaryServer.Domain.User;
import mark.DiaryServer.Domain.UserRelation;
import mark.DiaryServer.Service.DiaryCommentService;
import mark.DiaryServer.Service.MessageService;

public class MessageMethods {

	@Autowired
	private MessageService messageService;

	@Autowired
	private DiaryCommentService diaryCommentService;

	private <T extends Object> SystemMessage convert(T object) {
		SystemMessage systemMessage = new SystemMessage();
		User fromUser, toUser;
		Class domainClass;
		String domainId;
		if (object instanceof DiaryComment) {
			DiaryComment diaryComment = (DiaryComment) object;
			fromUser = diaryComment.getTextDomain().getFromUser();
			toUser = diaryComment.getLastComment().getFromUser();
			System.out.println(diaryComment.getLastComment().getFromUser());
			domainClass = DiaryComment.class;
			domainId = diaryComment.getId();
		} else if (object instanceof DiaryDigest) {
			DiaryDigest diaryDigest = (DiaryDigest) object;
			fromUser = diaryDigest.getFromUser();
			toUser = diaryDigest.getTextDomain().getFromUser();
			domainClass = DiaryDigest.class;
			domainId = diaryDigest.getDomainUuid();
		} else if (object instanceof UserRelation) {
			UserRelation userRelation = (UserRelation) object;
			fromUser = userRelation.getFromUser();
			toUser = userRelation.getToUser();
			domainClass = UserRelation.class;
			domainId = userRelation.getDomainUuid();
		} else {
			return null;
		}
		System.out.println(fromUser.getDomainUuid() + toUser.getDomainUuid());
		systemMessage.setCreateTime(new Date());
		systemMessage.setFromUser(fromUser);
		systemMessage.setReceiveUser(toUser);
		systemMessage.setDomainClass(domainClass);
		systemMessage.setDomainId(domainId);
		systemMessage.setIfCheck(false);
		return systemMessage;
	}

	public void saveMessageOnAfter(Object returnValue) {
		SystemMessage systemMessage = this.convert(returnValue);
		System.out.println("+++++++++++" + returnValue.toString());
		if (systemMessage == null) {
			System.out.println("无效的" + returnValue.toString());
			return;
		}

		this.messageService.addSystemMessage(systemMessage);

	}

}
